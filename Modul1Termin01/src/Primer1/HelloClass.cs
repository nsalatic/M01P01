﻿/*
Na početku svake klase (programske datoteke)
sledi using blok.
On sadrži spisak drugih prostora imena u kojima se nalaze
klase čijim funkcionalnostima se pristupa iz klase.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
Ovde se definise prostor imena koji služi za 
logičko grupisanje programskih datoteka.
Ne mora odgovarati fizičkoj hijerarhiji
ali se preporučuje!
*/
namespace Modul1Termin01.Primer1
{
    /*
    Ovde pocinje definicija klase, pre ovoga mogu biti 
    definisan prostor imena (namespace)
    Znacenje class cemo raditi kasnije
     */
    class HelloClass
    {
        // Izvrsenje programa pocinje od main metode
        // Klikom na Start dugme na toolbar-u ili F5
        public static void Main(string[] args)
        {
            // Linija ispod ispisuje tekst u zagradi
            // WriteLine je statička metoda klase Console
            // ali o tome cemo kasnije tokom kursa
            Console.WriteLine("Hello world!");
            // Da se konzola ne bi zatvorila nakon izvršavanja
            // u Debug režimu potrebno je dodati neku blokirajucu
            // proceduru. U ovom slučaju se čeka unos bilo kog 
            // karaktera sa tastature.
            Console.ReadKey();
            // Ako se program izvršava u Release režimu (Ctrl+F5)
            // onda nije potrebna blokirajuća procedura.
        }

    }
}
