﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul1Termin01.Primer2
{
    /*
    Ime klase je isto kao u prethodnom primeru, 
    ali to je samo kraći naziv. Njen pun naziv je prostor imena + naziv klase
    Trudimo se da je puno ime klase jedinstveno ne samo na nivou 
    naseg projekta, nego na globalnom nivou - pogledajte naziv paketa 
    */
    class HelloClass
    {
        public static void Main(string[] args)
        {
            //Deklaracija lokalne varijable name koja je tipa String
            string name;
            //Dodela string literala ".NET kurs" varijabli name
            //ovo moze da bude i odmah prilikom deklaracije
            name = ".NET kurs";
            //Konkatenacija stringova se vrsi pomocu operatora +
            Console.WriteLine("HelloWorld " + name + "!");
            //Primer konkatenacije stringova koja se smesta u lokalnu promenljivu
            //pa tek onda ispisuje
            string message = "HelloWorld " + name + "!";
            Console.WriteLine(message);
            //Specijalni karakteri se moraju eskejpovati pomocu \
            Console.WriteLine("HelloWorld \"" + name + "\"!");

            int brojRacunara = 7;
            double cena = 39000.52;
            bool imaCD = true;
            char os = 'W'; //tip instaliranog operativnog sistema

            Console.WriteLine("Broj racunara je: " + brojRacunara);
            Console.Write("\tCena: " + cena + "\n");
            Console.WriteLine("\tSa CD uredjajem?: " + imaCD);
            Console.WriteLine("\tOznaka operativnog sistema: " + os);

            //deklaracija i inicijalizacija vrednosti
            int a = 1;
            double b = 3.14;

            //implicitna konverzija, sa manjeg na veci tip MOZE
            Console.WriteLine("1. Stara vrednost b=" + b);
            b = a;

            Console.WriteLine("1. Nova vrednost b=" + b + "\n");

            //implicitna konverzija, sa veceg na manji tip NE MOZE
            //naredna linija vraca gresku, odkomentarisite kod
            //a = b;

            //eksplicitna konverzija moze, dolazi do gubitka podataka
            Console.WriteLine("2. Stara vrednost a=" + a);
            b = 3.14159265358979;
            a = (int)b;
            Console.WriteLine("2. Nova vrednost a=" + a + "\n");

            float f = (float)b;
            Console.WriteLine("2. Gubitak pri konverziji double broja " + b + " u float broj "+ f + "\n");

            //ARITMETICKI OPERATORI
            //operator + za sabiranje, isprobajte operatore - , * , /
            a = 1 + 1 - 5 * 9;
            b = a + 3.14;
            Console.WriteLine("3. Nova vrednost b=" + b + "\n");

            //prioritet operacija
            //da li je rezultat 4 ?, za definisanje prioriteta koristite zadrade
            a = 1 + 1 * 2;
            Console.WriteLine("4. Nova vrednost a=" + a + "\n");

            // koriscenje kombinovanog operatora a+=3 isto kao i a=a+3
            // isprobajte za -= , *=
            Console.WriteLine("5. Stara vrednost a=" + a);

            //a = a + 3;		
            a += 3;
            Console.WriteLine("5. Nova vrednost a=" + a + "\n");
            
            //a += 1;
            //a++;

            a = 10;
            b = ++a; //koliko je b?
            b = a++; //koliko je b?

            a = 10;
            b = a++ + ++a; //koliko je b?



            //razlika izmedju prefiksnog ++l i sufiksnog l++
            a = 4;
            Console.WriteLine("6. a je " + ++a + "\n");
            a = 4;
            Console.WriteLine("7. a je " + a++);

            Console.WriteLine("7. a je " + a + "\n");

            //operator %, ostatak pri deljenju
            b = a % 2;
            Console.WriteLine("8. Ostatak pri deljenju " + a + " i 2 je " + b);

            bool z = true;
            Console.WriteLine("Poredjenje: " + (z || (a == 5)));

            bool logicka = true;
            bool drugaLogicka = !logicka; //false

            bool trecaLogicka = logicka && drugaLogicka || logicka;
            //						true && false || true

            // nullable tipovi
            int? x = null; //probati i za neku konkretnu vrednost -5,5...
            int y = x ?? -1;
            Console.WriteLine("9. Vrednsot y je: " + y);

            // typeof
            Console.WriteLine("10. Tip varijable x je: " + x?.GetType()); //ako je x null onda nece ispisati nista, promeniti
            Console.WriteLine("11. Da li je y tipa int ? " + (y.GetType() == typeof(int)));

            int maxIntValue = 2147483647;

            int rez1 = unchecked(maxIntValue + 3);
            Console.WriteLine("12. Unchecked: " + rez1);
            int rez2 = checked(maxIntValue + 1);
            Console.WriteLine("12. Checked: " + rez2);

            Console.ReadKey();
        }
    }
}
