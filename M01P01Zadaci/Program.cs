﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M01P01Zadaci
{
    class Program
    {

        static void Main(string[] args)
        {
            //zadatak1
            double tezina = 45.5;
            string imePrezimeAdresaPos = "Salatic Nikola, Vladike Cirica 10";
            string imePrezimeAdresaPri = "Carapic Petar, Veselina Maslese 78";
            bool preporuceno = false;

            Console.WriteLine("Posaljilac: " + imePrezimeAdresaPos + ", salje paket primaocu: " + imePrezimeAdresaPri);
            Console.WriteLine("Tezina paketa je: " + tezina);
            if (preporuceno == false)
            {
                Console.WriteLine("Paket se ne salje preporuceno");
            }
            else
            {
                Console.WriteLine("Paket se salje preporuceno");
            }

            //zadatak2
            double stepeniC = 34;
            double stepeniF = stepeniC * 1.8 + 32;

            Console.WriteLine("Data temp " + stepeniC + " stepeni C iznosi: " + stepeniF + " u F");

            //zadatak3
            double r = 45;
            double O = 2 * r * Math.PI;
            double P = r * r * Math.PI;

            Console.WriteLine("Ako je evidentiran poluprecnik kruga" + r + ", O iznosi " + O + ", a P: " + P);

            //zadatak4
            double a = 45;
            double b = 12;
            double Obim = 2 * a + 2 * b;
            double Povrsina = a * b;

            Console.WriteLine("Ako imamo duzine stranica: " + a + " i " + b + ", O prav iznosi: " + O + ", a P: " + P);

            //zadatak5
            double cena = 45.2;
            int kolicina = 4;
            bool akcija = false;

            if (akcija == false)
            {
                Console.WriteLine("Ukupna cena je: " + cena * kolicina);
            }
            else
            {
                Console.WriteLine("Ukupna cena je: " + (cena * kolicina) * 0.9);
                Console.WriteLine("U cenu je uracunat akcijski popust");
            }

            //zadatak6
            int godina = 2012;

            if (godina % 400 == 0)
            {
                Console.WriteLine("Godina: " + godina + " je prestupna");
            }
            else if (godina % 400 != 0 && godina % 100 == 0)
            {
                Console.WriteLine("Godina: " + godina + " nije prestupna");
            }
            else if (godina % 100 != 0 && godina % 4 == 0)
            {
                Console.WriteLine("Godina: " + godina + " je prestupna");
            }
            else if (godina % 100 != 0 && godina % 4 != 0)
            {
                Console.WriteLine("Godina: " + godina + " nije prestupna");
            }
            else
            {
                Console.WriteLine("Pogresno uneta godina");
            }

            //zadatak7
            Console.WriteLine("Brojevi deljivi sa 5: ");
            for (int i = 1; i < 30; i++)
            {
                if (i % 5 == 0)
                {
                    Console.WriteLine(i);
                }
            }
            //zadatak8
            double x=45.56;
            double y=12.78;
            string operacija = "swap";

            switch(operacija)
            {
                case "min":
                    if(x<y)
                    {
                        Console.WriteLine("manji je x");
                    }
                    else
                    {
                        Console.WriteLine("manji je y");
                    }
                    break;
                case "max":
                    if(x>y)
                    {
                        Console.WriteLine("x je veci");
                    }
                    else
                    {
                        Console.WriteLine("y je veci");
                    }
                    break;
                case "swap":
                    double tempx = y;
                    double tempy = x;
                    x = tempx;
                    y = tempy;
                    Console.WriteLine("vrednosti su zamenjene");
                    Console.WriteLine("Sad je X: " + x + ", a Y: " + y);
                    break;
                case "equals":
                    if(x==y)
                    {
                        Console.WriteLine(0);
                    }
                    else if(x>y)
                    {
                        Console.WriteLine(1);
                    }
                    else
                    {
                        Console.WriteLine(-1);
                    }
                    break;
                default:
                    Console.WriteLine("Ne postoji data operacija");
                    break;
            }

            //zadatakDodatni
            //za unet broj praviti zvezdice
            

            Console.ReadLine();
        }
    }
}
