﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M01P01Domaci
{
    class Program
    {
        private static List<Vozila> listaVozila = new List<Vozila>();
        static void Main(string[] args)
        {
            string opcija;
            do
            {
                Console.WriteLine("Izaberite opciju");
                Console.WriteLine("*  Unos oglasa");
                Console.WriteLine("*  Brisanje oglasa");
                Console.WriteLine("1. Ispis svih oglasa");
                Console.WriteLine("2. Ispis oglasa po godini");
                Console.WriteLine("3. Ispis svih detalja po sifri");
                Console.WriteLine("4. Izmena oglasa po sifri");
                Console.WriteLine("5. Ispis po ceni");
                Console.WriteLine("6. Ispis po opremi");
                Console.WriteLine("7. Definisanje novog skupa opreme");
                Console.WriteLine("X  Za izlaz iz programa");
                Console.WriteLine();

                Console.WriteLine("Unesite opciju:");
                opcija = Console.ReadLine();

                switch (opcija)
                {
                    case "*":
                        Unos();
                        break;
                    case "1":
                        IspisSvih();
                        break;
                    case "2":
                        IspisPoGodini();
                        break;
                    case "3":
                        IspisSvega();
                        break;
                    case "4":
                        Izmena();
                        break;
                    case "5":
                        IspisPoCeni();
                        break;
                    case "6":
                        IspisPoOpremi();
                        break;
                    case "7":
                        NovaOprema();
                        break;
                    case "x":
                        Console.WriteLine("Izlaz");
                        Console.WriteLine();
                        break;
                    case "X":
                        Console.WriteLine("Izlaz");
                        Console.WriteLine();
                        break;
                    default:
                        Console.WriteLine("Pogresno uneta opcija");
                        Console.WriteLine();
                        break;
                }
            } while (!opcija.Equals("x") && !opcija.Equals("X"));
        }

        public static void Unos()
        {
            string sifra;
            string naslov;
            double cena;
            int godinaProizvodnje;
            List<string> listaOprema = new List<string>();

            Console.WriteLine("Unesite sifru oglasa:");
            sifra = Console.ReadLine();
            Console.WriteLine("Unesite naslov oglasa:");
            naslov = Console.ReadLine();
            Console.WriteLine("Unesite cenu vozila:");
            cena = Double.Parse(Console.ReadLine());
            Console.WriteLine("Unesite godinu proizvodnje");
            godinaProizvodnje = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Unos opreme");
            for (int i = 1; i <= 5; i++)
            {
                Console.WriteLine("Unesite " + i + ". element opreme:");
                string el = Console.ReadLine();
                listaOprema.Add(el);
            }

            Vozila vozilo = new Vozila(sifra, naslov, cena, godinaProizvodnje, listaOprema);
            listaVozila.Add(vozilo);
            Console.WriteLine("Oglas uspesno unet");
            Console.WriteLine();
        }

        public static void Brisanje()
        {
            Console.WriteLine("Unesite sifru oglasa:");
            string sifra = Console.ReadLine();
            bool ima = false;
            for(int i=0; i<listaVozila.Count; i++)
            {
                if(listaVozila.ElementAt(i).getSifra().Equals(sifra))
                {
                    ima = true;
                    listaVozila.RemoveAt(i);
                    Console.WriteLine("Brisanje uspesno");
                    Console.WriteLine();
                }
            }
            if(ima==false)
            {
                Console.WriteLine("Ne postoji oglas sa datom sifrom");
                Console.WriteLine();
            }
        }

        public static void IspisSvih()
        {
            Console.WriteLine("Lista oglasa");
            for (int i = 0; i < listaVozila.Count; i++)
            {
                Console.WriteLine(listaVozila.ElementAt(i).IspisSvih());
                Console.WriteLine("- - - - -");
            }
            Console.WriteLine();
        }

        public static void IspisPoGodini()
        {
            int godina;
            Console.WriteLine("Unesite godinu:");
            godina = Int32.Parse(Console.ReadLine());
            for (int i = 0; i < listaVozila.Count; i++)
            {
                if (listaVozila.ElementAt(i).getGodinaProizvodnje().Equals(godina))
                {
                    Console.WriteLine(listaVozila.ElementAt(i).IspisSvih());
                    Console.WriteLine("- - - - -");
                }
            }
            Console.WriteLine();
        }

        public static void IspisSvega()
        {
            Console.WriteLine("Unesite sifru oglasa:");
            string sifra = Console.ReadLine();

            for (int i = 0; i < listaVozila.Count; i++)
            {
                if (listaVozila.ElementAt(i).getSifra().Equals(sifra))
                {
                    Console.WriteLine("Podaci o trazenom oglasu:");
                    Console.WriteLine(listaVozila.ElementAt(i).IspisSvega());
                    Console.WriteLine("Oprema:");
                    for(int j=0; j<listaVozila.ElementAt(i).getLista().Count; j++)
                    {
                        Console.WriteLine(listaVozila.ElementAt(i).getLista().ElementAt(j));
                    }
                    Console.WriteLine("- - - - -");
                }
                else
                {
                    Console.WriteLine("Ne postoji oglas sa datim rednim brojem");
                    Console.WriteLine();
                }
            }
        }

        public static void Izmena()
        {
            bool ima = false;
            string opcija1;
            string sifra;
            List<string> listaOprema = new List<string>();
            Console.WriteLine("Unesite sifru oglasa:");
            sifra = Console.ReadLine();

            for (int i = 0; i < listaVozila.Count; i++)
            {
                if (listaVozila.ElementAt(i).getSifra().Equals(sifra))
                {
                    ima = true;
                    do
                    {
                        Console.WriteLine("1. Izmena osnovnih detalja");
                        Console.WriteLine("2. Izmena opreme");
                        Console.WriteLine("x  Za izlaz");
                        Console.WriteLine();

                        opcija1 = Console.ReadLine();
                        switch (opcija1)
                        {
                            case "1":
                                Console.WriteLine("Unesite novi naslov:");
                                string naslov = Console.ReadLine();
                                Console.WriteLine("Unesite novu cenu:");
                                double cena = Double.Parse(Console.ReadLine());
                                Console.WriteLine("Unesite novu godinu proizvodnje:");
                                int godina = Int32.Parse(Console.ReadLine());

                                listaVozila.ElementAt(i).setNaslov(naslov);
                                listaVozila.ElementAt(i).setCena(cena);
                                listaVozila.ElementAt(i).setGodinaProizvodnje(godina);
                                Console.WriteLine("Izmena uspesna");
                                Console.WriteLine();
                                break;
                            case "2":
                                for (int j = 1; j <= 5; j++)
                                {
                                    Console.WriteLine("Unesite " + j + ". element opreme:");
                                    string el = Console.ReadLine();
                                    listaOprema.Add(el);
                                }
                                listaVozila.ElementAt(i).setLista(listaOprema);
                                break;
                        }
                    } while (!opcija1.Equals("x") && !opcija1.Equals("X"));
                }
            }
            if (ima == false)
            {
                Console.WriteLine("Ne postoji oglas sa datom sifrom");
                Console.WriteLine();
            }
        }

        public static void IspisPoCeni()
        {
            string opcija2;
            do
            {
                Console.WriteLine("1. Pretraga po opsegu");
                Console.WriteLine("2. Pretraga od date cene");
                Console.WriteLine("3. Pretraga do date cene");
                Console.WriteLine("x  Za izlaz");
                Console.WriteLine("Unesite 1, 2 ili 3");
                Console.WriteLine();

                opcija2 = Console.ReadLine();
                switch (opcija2)
                {
                    case "1":
                        bool ima = false;
                        Console.WriteLine("Unesite pocetnu cenu:");
                        double pocetna = Double.Parse(Console.ReadLine());
                        Console.WriteLine("Unesite krajnju cenu:");
                        double krajnja = Double.Parse(Console.ReadLine());
                        Console.WriteLine();
                        for (int i = 0; i < listaVozila.Count; i++)
                        {
                            if (listaVozila.ElementAt(i).getCena() >= pocetna && listaVozila.ElementAt(i).getCena() <= krajnja)
                            {
                                ima = true;
                                Console.WriteLine(listaVozila.ElementAt(i).IspisSvih());
                                Console.WriteLine("- - - - -");
                            }
                            else
                            {
                                ima = false;
                            }
                        }
                        if (ima == false)
                        {
                            Console.WriteLine("Ne postoje oglasi u datom opsegu");
                            Console.WriteLine();
                        }
                        break;
                    case "2":
                        ima = false;
                        Console.WriteLine("Unesite cenu:");
                        pocetna = Double.Parse(Console.ReadLine());
                        Console.WriteLine();
                        for (int i = 0; i < listaVozila.Count; i++)
                        {
                            if (listaVozila.ElementAt(i).getCena() >= pocetna)
                            {
                                ima = true;
                                Console.WriteLine(listaVozila.ElementAt(i).IspisSvih());
                                Console.WriteLine("- - - - -");
                            }
                        }
                        if (ima == false)
                        {
                            Console.WriteLine("Ne postoje oglasi sa cenom iznad date");
                            Console.WriteLine();
                        }
                        break;
                    case "3":
                        ima = false;
                        Console.WriteLine("Unesite cenu:");
                        krajnja = Double.Parse(Console.ReadLine());
                        for (int i = 0; i < listaVozila.Count; i++)
                        {
                            if (listaVozila.ElementAt(i).getCena() <= krajnja)
                            {
                                ima = true;
                                Console.WriteLine(listaVozila.ElementAt(i).IspisSvih());
                                Console.WriteLine("- - - - -");
                            }
                        }
                        if (ima == false)
                        {
                            Console.WriteLine("Ne postoje oglasi sa cenom ispod date");
                            Console.WriteLine();
                        }
                        break;
                    default:
                        Console.WriteLine("Pogresna opcija");
                        Console.WriteLine();
                        break;
                }

            } while (!opcija2.Equals("x") && !opcija2.Equals("X"));
        }

        public static void IspisPoOpremi()
        {
            Console.WriteLine("Unesite opremu:");
            string oprema = Console.ReadLine();
            bool ima = false;

            for (int i = 0; i < listaVozila.Count; i++)
            {
                for (int j = 0; j < listaVozila.ElementAt(i).getLista().Count; j++)
                {
                    if (listaVozila.ElementAt(i).getLista().ElementAt(j).Equals(oprema))
                    {
                        ima = true;
                        Console.WriteLine(listaVozila.ElementAt(i).IspisSvih());
                        Console.WriteLine("- - - - -");
                    }
                }
            }
            if (ima == false)
            {
                Console.WriteLine("Trazenu opremu nema ni jedan oglas");
                Console.WriteLine();
            }
        }

        public static void NovaOprema()
        {
            bool ima = false;
            List<string> listaOprema = new List<string>();
            Console.WriteLine("Unesite sifru oglasa:");
            string sifra = Console.ReadLine();

            for (int i = 0; i < listaVozila.Count; i++)
            {
                if (listaVozila.ElementAt(i).getSifra().Equals(sifra))
                {
                    ima = true;
                    string oprema = "";
                    while (!oprema.Equals("x") && !oprema.Equals("X"))
                    {
                        Console.WriteLine("Unesite novi element opreme:");
                        oprema = Console.ReadLine();
                        if(oprema.Equals("x") || oprema.Equals("X"))
                        {
                            break;
                        }
                        listaOprema.Add(oprema);
                    }
                    listaVozila.ElementAt(i).setLista(listaOprema);
                    Console.WriteLine("Nova oprema uspesno uneta");
                    Console.WriteLine();
                }
            }
            if(ima==false)
            {
                Console.WriteLine("Ne postoji oglas sa datom sifrom");
                Console.WriteLine();
            }
        }
    }
}
