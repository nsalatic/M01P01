﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M01P01Domaci
{
    class Vozila
    {
        private static int brojac = 1;
        private int redniBroj;
        private string sifra;
        private string naslov;
        private double cena;
        private int godinaProizvodnje;
        private List<string> ListaOprema = new List<string>();

        public Vozila()
        {

        }

        public Vozila(string sifra, string naslov, double cena, int godinaProizvodnje, List<string> ListaOprema)
        {
            this.sifra = sifra;
            this.naslov = naslov;
            this.cena = cena;
            this.godinaProizvodnje = godinaProizvodnje;
            this.ListaOprema = ListaOprema;
            redniBroj = brojac ++;
        }

        public int getRedniBroj()
        {
            return redniBroj;
        }

        public string getSifra()
        {
            return sifra;
        }

        public string getNaslov()
        {
            return naslov;
        }

        public void setNaslov(string naslov)
        {
            this.naslov = naslov;
        }

        public double getCena()
        {
            return cena;
        }

        public void setCena(double cena)
        {
            this.cena = cena;
        }

        public int getGodinaProizvodnje()
        {
            return godinaProizvodnje;
        }

        public void setGodinaProizvodnje(int godinaProizvodnje)
        {
            this.godinaProizvodnje = godinaProizvodnje;
        }
        
        public List<string> getLista()
        {
            return ListaOprema;
        }

        public void setLista(List<string> ListaOprema)
        {
            this.ListaOprema = ListaOprema;
        }
        
        public string IspisSvih()
        {
            return "Redni broj: " + redniBroj + "\nSifra: " + sifra + "\nNaslov: " + naslov + "\nCena: " + cena;
        }

        public string IspisSvega()
        {
            return "Sifra: " + sifra + "\nNaslov: " + naslov + "\nCena: " + cena;
        }
    }
}
